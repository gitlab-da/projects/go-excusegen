# Xkcd 303 programmer's excuse generator

Simple image generator in Go, based on XKCD's famous [compiling comic](https://xkcd.com/303/).

> Note: This is a [fork of upstream](https://github.com/kruftmeister/go-excusegen) based on commit 5d81580 without the imgur feature. Code has been refreshed with Docker and GitLab CI/CD pipelines.

![Generated image for Pipeline Running](example-pipelinerunning.png)

# Requirements

* Docker or GitLab CI/CD

# How to use

Define the image.

```
IMAGE=registry.gitlab.com/gitlab-de/projects/go-excusegen:latest
```

Pull the container.

```
docker pull $IMAGE
```

Run the container to generate the `GitLab is down` image by default in `out/gitlab.png`.

```
docker run -ti --rm -v $PWD/out:/app/out $IMAGE

open out/gitlab.png
```

![example-excuse](example-gitlabdown.png)

You can customize the output with using `-long`, `-short` and `-out` parameters for the `/app/go-excusegen` binary. This overrides the default CMD entrypoint in the Docker container.

Example for `Pipeline running`:

```
docker run -ti --rm -v $PWD/out:/app/out \
  registry.gitlab.com/gitlab-de/projects/go-excusegen:latest \
  /app/go-excusegen -long "Pipeline running" -short "pipeline running" -out out/pipelinerunning.png
```

# Development

## Docker

```
$ IMAGE=go-excusegen

$ docker built -t $IMAGE .

$ docker run -ti --rm -v $PWD/out:/app/out $IMAGE
```

## Source

Requires a local Go build environment, e.g. with Homebrew on macOS.

```
$ go get ./...
$ go build ./...
```

```
$ ./go-excusegen -help
Usage of ./go-excusegen:
  -long string
    	long text for the excuse (default "compiling my code")
  -out string
    	Path to the generated PNG file. Defaults to out/out.png (default "out/out.png")
  -short string
    	short text for the excuse (default "compiling")
```

# License

This software is distributed under the terms of the MIT license. See LICENSE for details.

# Credit

Graphic resources (image & font) by [XKCD](https://xkcd.com)
