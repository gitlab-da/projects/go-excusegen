module gitlab.com/gitlab-de/go-excusegen

go 1.15

require (
	github.com/golang/freetype v0.0.0-20161208064710-d9be45aaf745
	golang.org/x/image v0.0.0-20170227160505-e6cbe778da3c
)
