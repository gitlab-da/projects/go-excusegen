package main

import (
	"bufio"
	"flag"
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"os"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/draw"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

const (
	size = 28.0
	DPI  = 72.0
)

var (
	long, short, out string
	f                *truetype.Font

	longTextBB = image.Rectangle{
		Min: image.Point{X: 60, Y: 75},
		Max: image.Point{X: 350, Y: 120},
	}
	shortTextBB = image.Rectangle{
		Min: image.Point{X: 140, Y: 215},
		Max: image.Point{X: 220, Y: 250},
	}
)

func main() {

	flag.StringVar(&long, "long", "compiling my code", "long text for the excuse")
	flag.StringVar(&short, "short", "compiling", "short text for the excuse")
	flag.StringVar(&out, "out", "out/out.png", "Path to the generated PNG file. Defaults to out/out.png")
	flag.Parse()

	// image template
	imgf, err := os.Open("./resources/xkcd-excuse-template.png")
	if err != nil {
		handleError("Error opening image", err)
	}
	defer imgf.Close()

	img, err := png.Decode(imgf)
	if err != nil {
		handleError("Error decoding image template", err)
	}

	var ok bool
	if img, ok = img.(*image.NRGBA); !ok {
		handleError("Could not type cast the image", err)
	}

	// fonts
	fontBytes, err := ioutil.ReadFile("./resources/xkcd.ttf")
	if err != nil {
		handleError("Error loading ttf file", err)
	}
	f, err = freetype.ParseFont(fontBytes)
	if err != nil {
		handleError("Error parsing font file", err)
	}

	dst := image.NewRGBA(img.Bounds())
	draw.Copy(dst, image.ZP, img, img.Bounds(), draw.Src, nil)

	// the complete excuse
	if err := drawString(`"`+long+`"`, size, &longTextBB, dst); err != nil {
		handleError("Cannot draw long string", err)
	}

	// the short excuse
	if err := drawString(short, size-2.0, &shortTextBB, dst); err != nil {
		handleError("cannot draw short string", err)
	}

	// save - allow to override the path
	outFile, err := os.Create(out)
	if err != nil {
		handleError("Error creating out file '"+out+"'", err)
	}
	defer outFile.Close()
	b := bufio.NewWriter(outFile)
	if err = png.Encode(outFile, dst); err != nil {
		handleError("Error encoding image file", err)
	}
	if err = b.Flush(); err != nil {
		handleError("Error flushing buffer", err)
	}

	fmt.Println("OK: Wrote image file '" + out + "'")
}

func handleError(text string, err error) {
	fmt.Println(text, err)
	os.Exit(1)
}

// drawString will try to draw the string text with size in the bounding box defined by bb on the image dst
// if bb is not nil then it will be checked whether the string wouldn't fit the bounding box,
// the size will be recalculated until it fits the bounding box
func drawString(text string, size float64, bb *image.Rectangle, dst *image.RGBA) error {
	s, startX := fitString(text, size, bb)
	fg := image.Black
	c := freetype.NewContext()
	c.SetDPI(DPI)
	c.SetFont(f)
	c.SetFontSize(s)
	c.SetClip(dst.Bounds())
	c.SetSrc(fg)
	c.SetDst(dst)
	c.SetHinting(font.HintingNone)

	pt := freetype.Pt(bb.Min.X, bb.Min.Y+(int(c.PointToFixed(size)>>6)))
	pt.X = startX
	_, err := c.DrawString(text, pt)
	return err
}

func fitString(text string, size float64, bb *image.Rectangle) (float64, fixed.Int26_6) {
	var adv fixed.Int26_6
	for {

		opts := &truetype.Options{
			Size: size,
			DPI:  DPI,
		}
		fFace := truetype.NewFace(f, opts)

		adv = font.MeasureString(fFace, text)
		bbMinXAsFixed := fixed.I(bb.Min.X)
		bbMaxXAsFixed := fixed.I(bb.Max.X)

		if bbMinXAsFixed+adv < bbMaxXAsFixed {
			break
		}
		size -= 1.0
	}

	bbWidth := bb.Max.X - bb.Min.X
	bbMiddle := bb.Min.X + bbWidth/2
	textStart := fixed.I(bbMiddle) - adv/2

	return size, textStart
}
