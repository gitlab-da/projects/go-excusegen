FROM golang:latest

RUN mkdir -p /app
COPY . /app
WORKDIR /app

RUN go get -d -v ./... && go build -v ./...

CMD /app/go-excusegen -long "GitLab is down" -short "GitLab down" -out out/gitlab.png

